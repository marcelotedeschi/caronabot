<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>CaronaBot</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">CaronaBot</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="create.php">Criar Carona</a></li>
                            <li class="active"><a href="delete.php">Deletar Carona</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">
                <div class="">
                    <?php
                        if (!isset($_POST["nome"])) { ?>
                            <h2>Digite o seu nome</h2>
                            <form action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nome</label>
                                    <input type="text" class="form-control" name="nome" id="exampleInputEmail1" placeholder="Nome">
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        <?php
                        }   else {

                            $servername = "localhost";
                            $username = "root";
                            $password = "root";
                            $dbname = "caronaBot";

                            // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
                            // Check connection
                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            }
                            $nome = $_POST["nome"];
                            $sql = "SELECT * FROM carona WHERE nome='$nome'";
                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                $error = 0;
                                // output data of each row
                                ?>
                                <table class="table">
                                    <tr>
                                        <th>#</th>
                                        <th>Nome</th>
                                        <th>Dia</th>
                                        <th>Hora</th>
                                        <th>Descricao</th>
                                    </tr>
                                <?php
                                while($row = $result->fetch_assoc()) {
                                ?>
                                    <tr>
                                        <td><?php echo $row["id"]?></td>
                                        <td><?php echo $row["nome"]?></td>
                                        <td><?php echo $row["dia"]?></td>
                                        <td><?php echo $row["hora"]?></td>
                                        <td><?php echo $row["descricao"]?></td>
                                    </tr><?php
                                }?>
                                </table>
                                <?php

                            } else {
                                $error = 1;
                                ?><h3>Não existem caronas cadastradas</h3>
                                    <p class="lead">
                                        <a href="delete.php" class="btn btn-lg btn-default">Voltar</a>
                                    </p>
                                <?php
                            }
                            $conn->close();

                            if (!isset($_POST["id"]) && ($error == 0)) { ?>
                                <h2>Selecione o ID para deletar</h2>
                                <form action="del.php" method="post" class="form-horizontal col-lg-4 col-lg-offset-4 ">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="id" placeholder="Digite o id selecionado">
                                    </div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                                <?php
                            }
                        }?>

                </div>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Created by <a href="https://marcelotedeschi.com" target="_blank">Marcelo T</a>, using <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>

