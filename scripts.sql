-- Script de insercao de caronas


-- Nome tem que estar no formato @username (do telegram)
-- dia tem que estar em ingles 
-- Hora no formato HH:MM
-- Descricao apenas uma string de no maximo 100 caracteres


INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Monday', '07:45', 'Minha Casa -> Ufscar at10');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Monday', '11:45', 'Ufscar at10 -> Minha Casa');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Tuesday', '07:45', 'Minha Casa -> Ufscar at9');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Tuesday', '11:45', 'Ufscar at9 -> Minha Casa');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Tuesday', '13:45', 'Minha Casa -> Ufscar DC');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Tuesday', '17:45', 'Ufscar DC -> Minha Casa');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Wednesday', '07:45', 'Minha Casa -> Ufscar at10');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Wednesday', '11:45', 'Ufscar at5 -> Minha Casa');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Wednesday', '13:45', 'Minha Casa -> Ufscar DC');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Wednesday', '17:45', 'Ufscar DC -> Minha Casa');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Thursday', '07:45', 'Minha Casa -> Ufscar at9');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Thursday', '11:45', 'Ufscar at9 -> Minha Casa');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Thursday', '13:45', 'Minha Casa -> Ufscar');

  INSERT INTO carona (nome, dia, hora, descricao)
  VALUES ('@marcelotedeschi', 'Thursday', '17:45', 'Ufscar -> Minha Casa');

