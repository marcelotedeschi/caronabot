<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>CaronaBot</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">CaronaBot</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="create.php">Criar Carona</a></li>
                            <li class="active"><a href="delete.php">Deletar Carona</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">
                <div><?php
                    $servername = "localhost";
                    $username = "root";
                    $password = "root";
                    $dbname = "caronaBot";

                    // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    $id = $_POST["id"];
                    // sql to delete a record
                    $sql = "DELETE FROM carona WHERE id='$id'";

                    if ($conn->query($sql) === TRUE) {
                        echo "Record deleted successfully";
                    } else {
                        echo "Error deleting record: " . $conn->error;
                    }

                    $conn->close();
                    ?>
                    <p class="lead">
                        <a href="delete.php" class="btn btn-lg btn-default">Voltar</a>
                    </p>

                </div>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Created by <a href="https://marcelotedeschi.com" target="_blank">Marcelo T</a>, using <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>

